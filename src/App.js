import './App.css';
import {QueryClient, QueryClientProvider, useQuery} from "react-query";
import News from "./News";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Header from "./Header";
import {Button} from "react-bootstrap";
import CategoryPicker from "./CategoryPicker";

const queryClient = new QueryClient();
function App() {

    return (
        <Router>
            <Header/>
            <CategoryPicker/>
            <Switch>
                <Route path="/businessNews">
                    <QueryClientProvider client={queryClient}>
                        <News category="business"/>
                    </QueryClientProvider>
                </Route>
                <Route path="/scienceNews">
                    <QueryClientProvider client={queryClient}>
                        <News category="science"/>
                    </QueryClientProvider>
                </Route>
                <Route path="/technologyNews">
                    <QueryClientProvider client={queryClient}>
                        <News category="technology"/>
                    </QueryClientProvider>
                </Route>
                <Route path="/">
                    <QueryClientProvider client={queryClient}>
                        <News category='general'/>
                    </QueryClientProvider>
                </Route>
            </Switch>
        </Router>

    );


}



export default App;
