import React from 'react';
import {BrowserRouter as Router, Link} from "react-router-dom";
import {Button} from "react-bootstrap";
import "./CategoryPicker.css";

const CategoryPicker = () => {
    return (
        <div className="categoryPickerContainer">
            <Link to="/businessNews">
                <Button className="categoryBtn">Business</Button>
            </Link>
            <Link to="/scienceNews">
                <Button className="categoryBtn">Science</Button>
            </Link>
            <Link to="/technologyNews">
                <Button className="categoryBtn">Technology</Button>
            </Link>

        </div>
    );
};

export default CategoryPicker;