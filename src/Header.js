import React from 'react';
import {Link} from "react-router-dom";
import "./Header.css";

function Header(props) {
    return (
        <div className="headerContainer">
            <Link className="homeLink" to="/">
                <h1>
                    b2News
                </h1>
            </Link>

        </div>
    );
}

export default Header;