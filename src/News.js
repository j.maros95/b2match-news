import React, {useState} from 'react';
import NewsList from "./NewsList";
import {useQuery} from "react-query";
import  "./News.css";



function News(props) {

    let newsData


    const {isLoading, error, data} = useQuery("news", () =>
        fetch('http://newsapi.org/v2/top-headlines?country=us&category=' + props.category + '&sortBy=publishedAt&pageSize=5&apiKey=462d92a7733345858b5247b4686fdfee').then(
            res => res.json()
        )

    )

    if(data) {
        console.log(data);
        newsData = data.articles;
    }


    if (isLoading) return <p>Loading...</p>;

    if (error) return 'An error has occurred: ' + error.message;



    return (

            <div className="newsContainer" >
                <NewsList news={newsData}/>
            </div>


    );
}

export default News;