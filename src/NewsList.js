import React, {useState} from 'react';
import './NewsList.css';
import {Button, Modal} from "react-bootstrap";
import NewsModal from "./NewsModal";


const NewsList = (props) => {

    const [showModal, setShow] = useState(false);
    const [newsData, setNewsData] = useState({});

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const handleNewsData = (data) => setNewsData(data);

    return (
        <div>
            {
                props.news.map((newsArticle, index) => (
                    <div className="newsListContainer" onClick={() => {
                        handleShow();
                        handleNewsData(newsArticle);
                    }}>
                        <img src={newsArticle.urlToImage} alt='news'></img>
                        <div>
                            <h2>{newsArticle.title}</h2>
                            <a href={newsArticle.url}>Source</a>
                        </div>
                    </div>
                ))
            }

            <NewsModal
                show={showModal}
                onHide={handleClose}
                data={newsData}
            />
        </div>
    );
};


export default NewsList;