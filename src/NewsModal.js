import React, {useState} from 'react';
import {Button, Modal} from "react-bootstrap";


const NewsModal = (props) => {



    return (
            <Modal show={props.show}
                   onHide={props.onHide}
                   data={props.data}
                   animation={false}
                   size="xl"
                   aria-labelledby="contained-modal-title-vcenter"
                   centered
            >
                <Modal.Header closeButton>
                    <Modal.Title>{props.data.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {props.data.content}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={props.onHide}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>

    );
};

;

export default NewsModal;